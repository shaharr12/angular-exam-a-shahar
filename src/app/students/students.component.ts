import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Student } from '../interfaces/student';
import { StudentsService } from '../students.service';


@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {
  addStudentFormOPen = false;
  
  matgrade:number;
  psycho:number;
  ispay:string;
  students:Student[];
  students$;
  result = [];
  return:string;
  isclick = false;
  constructor(private StudentsService:StudentsService, public authService:AuthService) { }
  


  add(student:Student){
    this.StudentsService.addStudent(student.matgrade,student.psycho,student.ispay); 
    this.addStudentFormOPen = false;
  }


  predict(matgrade:number,psycho:number,ispay:string,index:number){
    this.StudentsService.predict(matgrade,psycho,ispay).subscribe(
      res => {
        console.log(res);
        this.result[index] = res
        console.log(this.result[index]);
        
        
      })
      
      }

      deleteStudent(id:string){
        this.StudentsService.deleteStudent(id); 
      }

      
  
  ngOnInit(): void {
  
    this.students$ = this.StudentsService.getStudent();
    this.students$.subscribe(
      docs => {         
        this.students = [];
        var i = 0;
        for (let document of docs) {
          console.log(i++); 
          const student:Student = document.payload.doc.data();
          student.id = document.payload.doc.id;
             this.students.push(student); 
        }                        
      })
  }
}