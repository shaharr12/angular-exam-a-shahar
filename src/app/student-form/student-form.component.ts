import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Student } from '../interfaces/student';

@Component({
  selector: 'student-form',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {
  @Input() matgrade:number;  
  @Input() psycho:number; 
  @Input() ispay:string;
  @Input() id:string; 
  @Input() formType:string;
  errorMessage:string = ""
  @Output() closeEdit = new EventEmitter<null>();
  @Output() update = new EventEmitter<Student>();

  payedd:Object[] = [{id:1,name:'Yes'},{id:2,name:'No'}]
  pay:string;

  constructor() { }

  ngOnInit(): void {
  }

  tellParentToClose(){
    this.closeEdit.emit(); 
  }

  updateParent(){
    if (this.psycho < 200  || this.psycho >800 ){
      this.errorMessage = 'the psycho is out of range'
  }   
  else{ 
    let student:Student = {matgrade:this.matgrade, psycho:this.psycho ,ispay:this.ispay};
    this.update.emit(student); 
  }
  }
  }