import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {
  private url ="https://rodwf5rfi8.execute-api.us-east-1.amazonaws.com/ExamA-shahar"
  userCollection:AngularFirestoreCollection = this.db.collection('students'); 
  studentsCollection:AngularFirestoreCollection;

  getStudent(): Observable<any[]> {
    this.studentsCollection = this.db.collection(`students`)
    console.log(this.studentsCollection)
    return this.studentsCollection.snapshotChanges();    
  }


  addStudent(matgrade:number,psycho:number,ispay:string){
    const student = {matgrade:matgrade, psycho:psycho,ispay:ispay}
    this.db.collection(`students`).add(student);
  }


  predict(matgrade:number,psycho:number,ispay:string){
    let json = { 'matgrade': matgrade, 'psycho':psycho,'ispay':ispay
      }
      console.log(json)
    let body = JSON.stringify(json);
    return this.http.post<any>(this.url, body).pipe(
      map(res => {
        console.log(res);
        let final:string = res.result;
         console.log(final);
        return final; 
      })
    )

  }

  deleteStudent(id) {
    this.db.doc(`students/${id}`).delete();
  } 

  constructor(private db:AngularFirestore,private http:HttpClient) { }
}
