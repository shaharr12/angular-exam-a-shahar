export interface User {
    uid:string,
    email?: string | null,
    pgotoUrl?: string,
    displayName?: string
}
